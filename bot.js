require('dotenv').config();
const { prodPrefix, devPrefix, commandRegex, onkkoRegex, spaceRegex } = require('./config.json');
const { baroBot, roll, sortie, arbie, entrati, teshin, marketPrice, catFact, factBot, stream, automatedStream } = require("./lib/commands.js");
const { logCommand, fullMatch } = require("./lib/helpers.js")

const { streamMessage } = require("./lib/commands/stream")

const gifList = require('./lib/gifList')
const spaceList = require('./lib/spaceList')
const server = require('./lib/server.js')

// const Discord = require('discord.js');
// const client = new Discord.Client();
// ^ old way

// Require the necessary discord.js classes
const { Client, Events, GatewayIntentBits } = require('discord.js');
// Create a new client instance
const client = new Client(
    { intents: [
            GatewayIntentBits.Guilds, 
            GatewayIntentBits.GuildMessages,GatewayIntentBits.MessageContent
        ] }
);

const cron = require("node-cron")


// client.once('ready', ()=> {
//     console.log(`Ready!`);
// });

client.once(Events.ClientReady, c => {
	console.log(`Ready! Logged in as ${c.user.tag}`);
});
client.login(process.env.TOKEN);

// Switches the prefix when using `npm test` so we don't get doubled responses from Baro. Optional chaining on NODE_ENV so null is returned if 
const prefix = process.env.NODE_ENV?.trim() === "dev" ? devPrefix : prodPrefix

client.on('messageCreate', message => {
    if (message.content.charAt(0) === prefix) {
        // checks first for ! so we don't have to parse all messages
        const command = fullMatch(message.content, commandRegex);
        if (command) {
            logCommand(command[1], message)
            switch (command[1]) {
            case "baro":
                baroBot(message, command);
                break;
            case "roll":
                roll(message, command);
                break;
            case "sortie":
                sortie(message);
                break;
            case "muffin":
                message.channel.send(gifList.muffin);
                break;
            case "arbies":
            case "arbitrations":
            case "arbitration":
                arbie(message)
                break;
            case "arbys":
                message.channel.send(gifList.arbys)
                break;
            case "entrati":
                entrati(message)
                break;
            case "teshin":
                teshin(message)
                break;
            case "pc":
                marketPrice(message, command)
                break;
            case "catfact":
            case "catfacts":
                catFact(message)
                break;
            case "fact":
                factBot(message)
                break;
            case "stream":
                stream(message)
                break;
            case "test":
                automatedStream(client)
                break;
            default:
                console.log('nothing to see');
                message.channel.send(
                    `I don't recognize that command. Try \`!baro\`, \`!roll d20 d6\`, or \`!sortie\``
                );
            }
        }

    }
    else if (fullMatch(message.content, onkkoRegex)) {
        message.channel.send(
            {
                content: 'Saya… if he— if Onkko— if I could have shown her how catalogues of possible futures found out from the moment I could choose— could have chosen, to stay, none of them end— ended well.',
                tts: true
            }
        );
        message.channel.send(
            {
                content: 'None, save one: this one. The one where I left. [sigh] This future ends so well for her, for Konzu, for Cetus. As eviscerating a choice as it was, it was the only one I could live with.',
                tts: true
            }
        );
        message.channel.send(
            {
                content: 'Some day, I swear, this is— but— this was… this will be borne out. Speak of this no more. To be crushed by the singular excruciates. [sigh] What’s done is done. Enough.',
                tts: true
            }
        );
    }
    else if (fullMatch(message.content, spaceRegex) && !message.author.bot) {
        let numSpace = spaceList.length
        let rand = Math.floor(Math.random() * numSpace)
        let spaceQuote = spaceList[rand]
        message.channel.send({
            content: spaceQuote,
            tts: true
        })
        // console.log(spaceQuote)
    }
});

// Cron job
cron.schedule('0 9 * * *', () => {
    // ^ minute, hour, day of month, month, day of week
    // if there are 6 args, first is seconds
    automatedStream(client)
    let date = new Date()
    console.log(`run at ${date}`)
});

// test functions
async function testStream () {
    // automatedStream(client)
    console.log(await streamMessage())
}

server(); // runs the fileserver