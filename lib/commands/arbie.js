const { pandaRand } = require('../helpers')
const { statFetch } = require('../helpers/fetchFunctions')
const Discord = require('discord.js')

/**
 * Sends message to channel with arbitration data
 * @param {Message} message - Discord message. Used to get channel information for reply
 */
async function arbie(message) {
    message.channel.send(await arbieMsg());
}

async function arbieMsg() {

    const arbieData = await statFetch('arbitration');
    let { expiry, enemy, type, node } = arbieData
    const now = new Date().getTime()
    const expiresAt = new Date(expiry).getTime()
    const expiresIn = expiresAt - now // time left in ms
    const hours = Math.floor( expiresIn / (1000 * 60 * 60) ) 
    const minutes = 
        Math.floor( ( expiresIn - (hours * 1000 * 60 * 60) ) / (1000 * 60) )
    
     let msg = 
        new Discord.MessageEmbed()
        .setColor('#0099ff')
        .setTitle('Current Arbitration')
        .addFields(
            { name: `**${enemy} ${type}**`, value: `${node}` },
            { name: `\u200B`, value: `Ends in **${hours} hours ${minutes} minutes**` },
        )
        .setImage(pandaRand())
    return msg
}


module.exports = arbie