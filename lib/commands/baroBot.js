const { statFetch } = require('../helpers/fetchFunctions')
const { estFormat } = require('../helpers')
const { cosmeticRegex } = require('../../config.json')

/**
 * Multifunctional command for Baro and his wares. If called with no options, sends message with info about his comings and goings. Use `wares` or `cosmetics` options for price data about his current offerings.
 * @param {Message} message - Discord message. Used to get channel information
 * @param {Array} params - Array containing:
 * - [0] - full text of command
 * - [1] - the command
 * - [2] - the optional arguments of the command
 */
async function baroBot(message, params) {
    const options = params[2] ? params[2] : null;
    if (!options) {
        message.channel.send(await baroMsg());
    }
    else {
        const upsell = message.content.match(/sandwich|pizza/);
        const wares = message.content.match(/wares|cosmetics/);
        const cosmetics = message.content.includes('cosmetics');
        if (upsell) {
            message.channel.send(
                `I've got **Primed Subs**, **Primed Heroes**, and **Primed Cubans**. Send 🍕 to Albert for details.`
            );
        } else if (wares) {
            message.channel.send(await baroWares(message, cosmetics));
        } else {
            message.channel.send(
                `\`!baro\` with no arguments will fetch when he is coming and going; you can also add \`wares\` or \`cosmetics\` to see what he has. Ping Albert with requests for functionality.`
            );
        }
    }
}

/**
 * Returns ETA or ETD data when no option is used on `!baro` command
 */
async function baroMsg () {
    const baroData = await statFetch('voidTrader');
    console.log(baroData)
    if (!baroData) 
        {
            return `Something went wrong. Try again in a bit. [baroData was empty]`}
    else {
        // arrival data
        const now = new Date()
        const arrival = new Date(await baroData.activation)
        const leaving = new Date(await baroData.expiry)
        const eta = await baroData.startString
        const etd = await baroData.endString
        const isHere = arrival <= now

        if (isHere) {
            return `Baro is here until **${estFormat(leaving)}**, which is in **${etd}**`
        }
        else {
            return `Baro will be coming in **${eta}**, at **${estFormat(arrival)}**`
        }
    }
}

/**
 * Returns Baro's offerings with price data. If `wares` options is used on `!baro`, returns non-cosmetics. If `cosmetics` option is used, returns cosmetics.
 * @param {Message} message - Discord message. Used to get channel information
 * @param {boolean} cosmetics - True if `cosmetics` option is used
 */
async function baroWares (message, cosmetics) {
    const baroData = await statFetch('voidTrader');
    const inventory = await baroData.inventory
    const emojiList = message.guild.emojis.cache
    const ducat = emojiList.find(emoji => emoji.name == "ducat") 
    const credit = emojiList.find(emoji => emoji.name == "credit") 
    console.log(`cosmetics value = ${cosmetics}`)

    let msg = !cosmetics ? 'Baro currently has these items for sale:\n' : 'Baro currently has these cosmetics for sale:\n' 

    inventory.forEach(ware => {
        const { item, ducats, credits } = ware
        const isCosmetic = item.match(new RegExp(cosmeticRegex)) ? true : false
        if (cosmetics == isCosmetic) {
            msg += `__**${item}**__: *${ducats}* ${ducat} & *${credits}* ${credit} \n`
        }
    })
    console.log(msg.length)
    return msg

}

module.exports = baroBot