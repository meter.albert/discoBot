const { fetchWithTimeout } = require('../helpers/fetchFunctions')

async function catFact(message) {
    message.channel.send(await catMsg());
}

async function catMsg () {
    let catfactsUrl = "https://catfact.ninja/fact"
    let data
    await fetchWithTimeout(catfactsUrl, 5000)
        .then(res => res.json())
        .then(json => data = json)
        .catch(err => console.log(err))
    let { fact } = data
    
    return fact
}

module.exports = catFact