const { statFetch } = require('../helpers/fetchFunctions')

async function entrati(message) {
    message.channel.send(await entratiMsg());
}

async function entratiMsg() {

    const syndicateData = await statFetch('syndicateMissions')
    const entratiData = await syndicateData.find(item => item.syndicate == "Entrati")
    const {eta} = entratiData
    const vaults = await entratiData.jobs.filter(item => item.type.includes("Isolation Vault"))

    const necramechPiece = vaults[0].rewardPool.filter(item => item.includes("Damaged Necramech"))[0]

    let msg = `**${necramechPiece}** is currently dropping from Entrati bounties. Time remaining until next rotation starts: **${eta}** *(Cycle is Barrel > Receiver > Stock)*`
    return msg 
}

module.exports = entrati