const factList = require('../factList')

async function factBot(message) {
    message.channel.send(await factMsg())
}

let factMsg = () => {
    let numFacts = factList.length
    let rand = Math.floor(Math.random() * numFacts)
    let fact = factList[rand]
    // console.log(fact)
    return fact
}

module.exports = factBot