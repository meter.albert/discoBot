import numpy as np
import pandas as pd

# print(np.pi)
nerd = [
	[ 's.prop1', 'customMetadata.s.prop1' ],
	[ 's.prop2', 'customMetadata.s.prop2' ],
	[ 's.prop3', 'customMetadata.s.prop3' ],
	[ 's.prop4', 'customMetadata.s.prop4' ],
	[ 's.prop5', 'customMetadata.s.prop5' ],
	[ 's.prop6', 'customMetadata.s.prop6' ],
	[ 's.prop7', 'customMetadata.s.prop7' ],
	[ 's.prop8', 'customMetadata.s.prop8' ],
	[ 's.prop10', 'customMetadata.s.prop10' ],
	[ 's.prop11', 'customMetadata.s.prop11' ],
	[ 's.prop15', 'customMetadata.s.prop15' ],
	[ 's.prop20', 'customMetadata.s.prop20' ],
	[ 's.prop22', 'customMetadata.s.prop22' ],
	[ 's.prop23', 'customMetadata.s.prop23' ],
	[ 's.prop24', 'customMetadata.s.prop24' ],
	[ 's.prop27', 'customMetadata.s.prop27' ],
	[ 's.prop38', 'customMetadata.s.prop38' ],
	[ 's.prop41', 'customMetadata.s.prop41' ],
	[ 'eVar1', 'customMetadata.eVar1' ],
	[ 'eVar2', 'customMetadata.eVar2' ],
	[ 'eVar3', 'customMetadata.eVar3' ],
	[ 'eVar4', 'customMetadata.eVar4' ],
	[ 'eVar5', 'customMetadata.eVar5' ],
	[ 'eVar6', 'customMetadata.eVar6' ],
	[ 'eVar7', 'customMetadata.eVar7' ],
	[ 'eVar8', 'customMetadata.eVar8' ],
	[ 'eVar10', 'customMetadata.eVar10' ],
	[ 'eVar11', 'customMetadata.eVar11' ],
	[ 'eVar15', 'customMetadata.eVar15' ],
	[ 'eVar20', 'customMetadata.eVar20' ],
	[ 'eVar22', 'customMetadata.eVar22' ],
	[ 'eVar23', 'customMetadata.eVar23' ],
	[ 'eVar24', 'customMetadata.eVar24' ],
	[ 'eVar27', 'customMetadata.eVar27' ],
	[ 'eVar38', 'customMetadata.eVar38' ],
	[ 'eVar41', 'customMetadata.eVar41' ],
	[ 'h:sc:ssl', 'params.analytics.enableSSL' ],
	[ 'l:asset:ad_length', 'params.media.ad.length' ],
	[ 'l:asset:length', 'n/a', '' ],
	[ 'l:asset:pod_offset', 'params.media.ad.podIndex' ],
	[ 'l:event:duration', 'n/a', 'Probably implementation detail' ],
	[ 'l:event:playhead', 'playerTime.playhead' ],
	[ 'l:event:ts', 'playerTime.ts' ],
	[ 'l:sp:hb_api_lvl', 'n/a', 'Probably implementation detail' ],
	[ 'l:stream:bitrate', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:dropped_frames', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:fps', 'n/a', 'Not used in legacy.' ],
	[ 'l:stream:startup_time', 'n/a', 'Not used in legacy.' ],
	[ 'n/a', 'l:aam:loc_hint', 'not sure what this is' ],
	[ 'n/a', 'params.media.ad.playerName' ],
	[ 'n/a', 'params.media.ad.podFriendlyName', 'Not in legacy implementation. Pod seems to be named after the ad.'],
	[ 'n/a', 'params.media.ad.podSecond' ],
	[ 's:aam:blob', 'n/a', 'Not sure what this is' ],
	[ 's:aam:blob', 'n/a', 'Not sure what this is' ],
	[ 's:asset:ad_id', 'params.media.ad.id' ],
	[ 's:asset:ad_name', 'params.media.ad.name' ],
	[ 's:asset:ad_sid', 'n/a', 'missing mapping' ],
	[ 's:asset:name', 'n/a', 'Redundant with friendlyName' ],
	[ 's:asset:pod_id', 'n/a' ],
	[ 's:asset:pod_name', 'n/a' ],
	[ 's:asset:pod_position', 'params.media.ad.podPosition', 'Not in legacy implementation.' ],
	[ 's:asset:publisher', 'params.visitor.marketingCloudOrgId' ],
	[ 's:asset:type', 'n/a', 'Not sure what this is' ],
	[ 's:asset:video_id', 'params.media.assetId' ],
	[ 's:event:sid', 'n/a', 'Probably implementation detail' ],
	[ 's:event:type', 'eventType' ],
	[ 's:meta:a.media.airDate', 'params.media.firstAirDate' ],
	[ 's:meta:a.media.asset', 'n/a', 'Redundant with params.media.assetId' ],
	[ 's:meta:a.media.episode', 'params.media.episode' ],
	[ 's:meta:a.media.feed', 'params.media.feed' ],
	[ 's:meta:a.media.format', 'n/a', 'Has value of vod, not sure if relevant' ],
	[ 's:meta:a.media.friendlyName', 'params.media.name' ],
	[ 's:meta:a.media.genre', 'params.media.genre' ],
	[ 's:meta:a.media.length', 'params.media.length' ],
	[ 's:meta:a.media.name', 'params.media.id' ],
	[ 's:meta:a.media.playerName', 'params.media.playerName' ],
	[ 's:meta:a.media.rating', 'params.media.rating' ],
	[ 's:meta:a.media.season', 'params.media.season' ],
	[ 's:meta:a.media.show', 'params.media.show' ],
	[ 's:meta:a.media.streamType', 'n/a', 'Not sure this is relevant' ],
	[ 's:meta:s.channel', 'customMetadata.s.channel', 'Miguel also implemented this as params.media.channel' ],
	[ 's:meta:s.pageName', 'customMetadata.s.pageName', '' ],
	[ 's:sc:rsid', 'params.analytics.reportSuite' ],
	[ 's:sc:tracking_server', 'params.analytics.trackingServer' ],
	[ 's:sp:hb_version', 'n/a', 'Probably implementation detail' ],
	[ 's:sp:player_name', 'n/a', 'Probably implementation detail' ],
	[ 's:sp:sdk', 'n/a', 'Probably implementation detail' ],
	[ 's:stream:type', 'params.media.contentType' ],
	[ 's:user:mid', 'n/a', 'Probably implementation detail' ],
	[ 's:user:mid', 'n/a', 'Probably implementation detail' ],
]

# print(nerd[1])
df = pd.DataFrame(nerd)

