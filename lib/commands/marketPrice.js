const { fetchWithTimeout } = require('../helpers/fetchFunctions')
const { titleCase, lowerCase } = require("../helpers/formatting");
const Discord = require('discord.js');

async function marketPrice(message, command) {
    message.channel.send(await priceMsg(command));
    // Note: [2] is all of the parameters
}

/**
 * @returns Stats for the searched for item based on the last 90 days
 * @param {*} command [2] contains the arguments to use (note that this is not plit)
 */
async function priceMsg (command) {
    let urlStart = "https://api.warframe.market/v1/items/"
    let urlEnd = "/statistics"
    let lowercaseItemName = lowerCase(command[2])
    let underscoredItemName = lowercaseItemName.split(" ").join("_")
    let formattedName = titleCase(lowercaseItemName)
    console.log(formattedName)
    let fullUrl = urlStart + underscoredItemName + urlEnd

    let data
    await fetchWithTimeout(fullUrl, 5000)
        .then(res => res.json())
        .then(json => data = json)
        .catch(err => console.log(err))
    
    let stat90Day = await data.payload.statistics_closed['90days']
    let days = stat90Day.length
    let { min_price, max_price, avg_price, moving_avg, volume } = stat90Day[days - 1]
    let msg = 
        new Discord.MessageEmbed()
        .setColor('#0099ff')
        .setTitle(`${formattedName}`)
        .setDescription(`Rolling average based on last 90 days. ${volume} items were sold yesterday.`)
        .addFields(
            { name: `**Min Price**`, value: `${min_price}` },
            { name: `**Max Price**`, value: `${max_price}` }, 
            { name: `**Average**`, value: `${avg_price}` },
            { name: `**Rolling Average**`, value: `${moving_avg}` },
        )
    return msg
}

module.exports = marketPrice