const gifList = require('../gifList');

/**
 * Rolls dice. Arguments are 
 * @param {Message} message - Discord message. Required to get channel to reply to
 * @param {*} params - Full text of message after !
 * @returns { string } - Message with number of dice rolled, the faces, and the sum
*/
function roll(message, params) {
    // 2nd index of this array is the stuff following !command
    const stuffToRoll = params[2]?.split(" ") ?? null;
    // in case there are no d# values 
    const dice = stuffToRoll.filter(item => {
        return item.match(/d\d+/);
    });
    // console.log(dice)
    if (dice.length > 0) {
        // roll each die in dice array
        const rolls = dice.map(die => {
            // faces interprets d# as # sided die
            let faces = parseInt(die.match(/\d+/)[0]);
            // need to adjust for possible 0 from floor of a number < 1
            return Math.floor(Math.random() * (faces - 1)) + 1;
        });

        // text handling for how many dice rolled
        const numDice = dice.length == 1 ? "once" : `${dice.length} times`;
        // sums up rolls
        const total = rolls.reduce((a, b) => a + b);

        message.channel.send(
            `You rolled ${numDice} for a total of (${rolls.join(" + ")}) = ${total}`
        );
    }
    else {
        message.channel.send(
            `Test: Not a valid roll. Follow \`!roll\` with your dice. (e.g. \`!roll d20 d5\`)`
        );
    };
    if (stuffToRoll.some(item => /joint/.test(item))) {
        message.channel.send(gifList.snoop);
    }
}

module.exports = roll