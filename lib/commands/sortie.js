const { getQuote, pandaRand } = require('../helpers')
const { statFetch } = require('../helpers/fetchFunctions')
const Discord = require('discord.js')

async function sortie(message) {
    message.channel.send(await sortieMsg());
}

async function sortieMsg () {
    const sortieData = await statFetch('sortie');
    const missions = await sortieData.variants

   let msg = 
        new Discord.MessageEmbed()
        .setColor('#0099ff')
        .setTitle('Sorties')
        .addFields(
            { name: `Stage 1: **${missions[0].missionType}** - (${missions[0].node})`, value: `*${missions[0].modifier}*` },
            { name: `Stage 2: **${missions[1].missionType}** - (${missions[1].node})`, value: `*${missions[1].modifier}*` },
            { name: `Stage 3: **${missions[2].missionType}** - (${missions[2].node})`, value: `*${missions[2].modifier}*` },
            { name: `\u200B`, value: `Ends in **${sortieData.eta}**` },
        )
        .setImage(pandaRand())
        .setFooter(await getQuote())
    return msg
}

module.exports = sortie