const { intervalToDuration, formatDuration } = require('date-fns');
const { twitchAuth, getNextStream } = require('../helpers/fetchFunctions');
const { estFormat } = require('../helpers');
require('dotenv').config();
const Discord = require('discord.js');
const schedule = require('node-schedule')

const authURL = 'https://id.twitch.tv/oauth2/token';
const client_id = process.env.TWITCH_BAROBOT_CLIENT_ID;
const client_secret = process.env.TWITCH_BAROBOT_CLIENT_SECRET;
const access_token = twitchAuth(authURL, client_id, client_secret);

/**
 * Sends message to channel with upcoming stream info
 * @param {*} message Discord command
 */
async function stream(message) {
    const { msg, timeUntilStream, start_time } = await streamMessage();
    
    if (timeUntilStream?.days === 0) {
        console.log(timeUntilStream)
        reminder(message, start_time)
    } 
    message.channel.send(msg);
    
}

/**
 * Sends message to General channel if there is a stream that day
 * @param {Object} client  Discord Client
 */
async function automatedStream(client) {
    // general id: 791486393745408022
    // testing id: 802753623326588938
    const { msg, timeUntilStream, start_time } = await streamMessage();
    
    if (timeUntilStream?.days === 0) {
        reminder(msg, start_time, client)
        client.channels.cache.get(`791486393745408022`).send(msg)
    }
}

/**
 * Responds to last bot message with alarm clock reaction
 * Users that react to the message will get a notification 15 min before the stream
 * @param {*} msg Discord message
 */
async function reminder(msg, start_time, client) {
    const messageFilter = m => m.author.bot === true
    
    // the automated stream doesn't have a triggering message, so need to use client and feed the hard coded channel id
    let target = client ? client.channels.cache.get(`791486393745408022`) : msg.channel
    
    const collector = target.createMessageCollector(
        messageFilter, { max: 1, time: 2000 }
    )
    
    collector.on('collect', async m => {
        m.react('⏰')
        // second arg for ReactionCollector is a filter (seems to be necessary)
        const rxnCollector = new Discord.ReactionCollector(m, _ => true, {time: 1800000 })
        rxnCollector.on('collect', rxn => {
            // filtering out bots; not sure if this is possible in the parent
            let notbots = rxn.users.cache.filter(user => user.bot === false)
            if (notbots.last()){
                // console.log(notbots.last())               
                m.channel.send(`<@${notbots.last().id}>, I'll remind you 15 minutes before the stream begins.`)

                let reminderTime = new Date(start_time)
                reminderTime.setSeconds(reminderTime.getSeconds() - 900)
                console.log(reminderTime)
                
                schedule.scheduleJob( reminderTime, () => { 
                    m.channel.send(`<@${notbots.last().id}>, stream will be starting in 15 minutes.`)
                    // console.log(new Date())
                })
            }
        })
        rxnCollector.on('end', e => {
            // should end 15 minutes after message
            console.log("Collection ended")
        })  
    })
}

/**
 * Calls the getNextStream function, which returns stream info
 * @returns Object containing message and time left until stream starts
 */
async function streamMessage() {
    const streamURL = 'https://api.twitch.tv/helix/schedule';
    const nextStream = await getNextStream(streamURL, client_id, await access_token);

    // in case there are no streams. 
    if (!nextStream?.id) { 
        return {
            msg: `No more streams this week.`,
            timeUntilStream: {}
        }
    }

    const { start_time, end_time, title } = nextStream;

    const timeUntilStream = intervalToDuration({ start: new Date(), end: new Date(start_time) });

    const formattedTimeUntilStream = formatDuration(timeUntilStream);

    return {
        msg: `**Next stream** (${title}) will be **${estFormat(start_time)}** (${formattedTimeUntilStream} from now). \nhttps://www.twitch.tv/warframe`,
        timeUntilStream,
        start_time
    };
}

module.exports = { stream, automatedStream, streamMessage }
