const { intervalToDuration, formatDuration } = require('date-fns');
const { statFetch } = require('../helpers/fetchFunctions')
const { estFormat } = require('../helpers')

async function teshin(message) {
    message.channel.send(await teshinMessage());
}

/**
 * @returns Teshin's current and next offering and expiration of offer
 */
 async function teshinMessage() {
    const { currentReward, expiry, rotation } = await statFetch('steelPath')
    let now = new Date()
    let duration = intervalToDuration(
        {start: now, end: new Date(expiry)}
    )
    let formattedDuration = formatDuration(duration)
    let getNextRewardIndex = () => {
        for (i=0; i < rotation.length; i++){
            if (rotation[i].name === currentReward.name) {
                if (i === rotation.length - 1){
                    // i.e. item is last in array
                    return 0
                } else return i + 1
            }
        }
    }
    let nextIndex = getNextRewardIndex()
    return `Teshin is currently offering **${currentReward.name}** for *${currentReward.cost} Steel Essences* until **${estFormat(expiry)}** (${formattedDuration} from now). \nThe following offering will be **${rotation[nextIndex].name}** for *${rotation[nextIndex].cost} Steel Essences*`

}

module.exports = teshin