const { format, utcToZonedTime } = require('date-fns-tz')

/**
 * Forces times to be Eastern Time
 * @param {string} date 
 * @returns String in M/d/yyyy h:mm:ss a format in EST
 */
const estFormat = (date) => {
    let nyTime = utcToZonedTime(date, 'America/New_York')
    
    return format(nyTime, 'M/d/yyyy h:mm:ss a')
}

module.exports = estFormat