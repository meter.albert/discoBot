const fetch = require('node-fetch')
const { Headers } = fetch
const AbortController = require('abort-controller')

const fetchWithTimeout = async (resource, timeout = 1000) => {
    // adapted from https://dmitripavlutin.com/timeout-fetch-request/
    const controller = new AbortController();
    const id = setTimeout(() =>
        controller.abort(), // sends abort signal if timeout reached
        timeout
    )
    // attaches controller to fetch req
    const response = await fetch(resource, {signal: controller.signal})
    
    clearTimeout(id) // stops timeout if fetch successful
    return response
}

const url = 'https://api.warframestat.us/pc/'
const statFetch = async (path) => {
    let data
    await fetchWithTimeout(`${url}${path}`, 5000)
        .then(res => res.json())
        .then(json => data = json)
        .catch(err => console.log(err))
    return data
}


/**
 * @returns Access token for Twitch
 */
const twitchAuth = async (authURL, client_id, client_secret) => {

    const authURLWithParams = 
        `${authURL}?client_id=${client_id}&client_secret=${client_secret}&grant_type=client_credentials`

    const requestOptions = {
        method: 'POST',
        redirect: 'follow'
    };
      
    let access_token
    
    await fetch(authURLWithParams, requestOptions)
        .then(res => res.json())
        .then(json => access_token = json.access_token)
    return access_token
}

/**
 * Uses app token to get next stream
 * @returns Stream object
 */
async function getNextStream (scheduleURL, client_id, access_token) {
    const broadcaster_id = "31557216" // hardcoding since this won't change

    const myHeaders = new Headers();
    myHeaders.append("Client-Id", client_id);
    myHeaders.append("Authorization", "Bearer " + access_token);

    const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    const scheduleURLwithParams = 
        `${scheduleURL}?broadcaster_id=${broadcaster_id}`
    // start time defaults to current date and time, so past streams should already be filtered out
    let nextStream

    await fetch(scheduleURLwithParams, requestOptions)
        .then(res => res.json())
        .then(json => nextStream = json.data.segments?.[0])
        // added optional chaining in case segments is empty
    return nextStream
    
}


module.exports = { 
    fetchWithTimeout,
    statFetch,
    twitchAuth,
    getNextStream
}
