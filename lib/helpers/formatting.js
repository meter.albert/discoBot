/**
 * Capitalizes the first letter of each word in a string
 * @returns String
 */
let titleCase = (str) => {
    let words = str.split(" ");
    let capitalized = words.map(word => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    });
    return capitalized.join(" ");
};

let lowerCase = (str) => {
    let words = str.split(" ");
    let lower = words.map(word => {
        return word.toLowerCase();
    });
    return lower.join(" ");
}

module.exports = { titleCase, lowerCase }