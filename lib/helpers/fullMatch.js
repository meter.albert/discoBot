/**
 * Helper for determining if there is a match, returns null if not
 * @param {string} str - String to match against
 * @param {RegExp} regexStr - String to create new RegExp from
 * @returns {Array} - If result is not null, [0] is the full match
 */
function fullMatch(str, regexStr) {
    const regex = new RegExp(...regexStr);
    return str.match(regex) ?? null;
}

module.exports = fullMatch