const { fetchWithTimeout } = require("./fetchFunctions");

async function getQuote() {
    let quotes = '';

    await fetchWithTimeout('https://type.fit/api/quotes')
        .then(res => res.json())
        .then(json => quotes = json)
        .catch(err => console.log(err));
    try {
        let picker = Math.floor(Math.random() * quotes.length);
        return `${quotes[picker].text} \n- ${quotes[picker].author}`;
    }
    catch (err) {
        console.log(`Couldn't fetch from quotes url. Error: '${err}'`);
        return '';
    }
}

module.exports = getQuote