const { format } = require('date-fns')
const fs = require('fs')
const logPath = './logs/test.txt'

/**
 * Logs date and command whenever a command is used
 * @param {string} command - Command triggered
 * @param {object} message - Discord message. Used to fetch the user and message contents
 */
const logCommand = (command, message) => {
    
    // need to prepend newline to front of command
    let time = format(new Date(), 'M/d/yyyy h:mm:ss a', { timeZone: 'America/New_York' })
    let user = message.author.username
    let messageContent = message.content

    let content = `\n${time}: ${command} command used by ${user}. Full message content: \n\t{${messageContent}}`
    console.log(content)


    fs.writeFile(
        logPath, content, { flag: 'a'}, err => {
        if (err) {
            console.log(err)
            return
        }
    })

}

module.exports = logCommand