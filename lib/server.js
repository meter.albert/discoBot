require('dotenv').config();
const fs = require('fs')
const path = require('path')

/**
 * Sets up server for serving panda pics at /panda endpoint
 */
require('dotenv').config()
const port = process.env.PORT || 8000
const express = require('express');
const app = express();

const pandaDir = '/../static/pandamode'

let server = () => {
    // serve static files
    app.use('/', express.static('static/'));
    
    // allows use of pandamode folder
    app.use('/panda/', (req, res) => {
        res.sendFile(
          path.resolve(
            `${__dirname}${pandaDir}/${randomImage(pandaDir)}`
          )
        )
    })
    
    // error catcher
    app.use('/*', (req, res) => {
      res.send('This page does not exist')
    })

    app.listen(port, () => {
        console.log(`App listening on port ${port}!`)
      });
      
    function randomImage(imgPath) {
        // picks random file in folder and returns its name
        const files = fs.readdirSync(
          path.resolve(__dirname + imgPath)
        )
        const chosenFile = files[Math.floor(Math.random() * files.length)]
        // console.log(chosenFile)
        return chosenFile
    }
}

module.exports = server