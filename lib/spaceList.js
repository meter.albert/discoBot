let spaceList = [
    "(excited gasps)",
    "[humming]",
    "Ah!",
    "Are we in space yet? What's the hold-up? Gotta go to space. Gotta go to SPACE.",
    "Are we in space?",
    "Atmosphere. Black holes. Astronauts. Nebulas. Jupiter. The Big Dipper.",
    "Ba! Ba! Ba ba ba! Space! Ba! Ba! Ba ba ba!",
    "Ba! Ba! Ba ba ba! Space!",
    "Bam, Bam bam bam! Take that, space.",
    "Better buy a telescope. Wanna see me. Buy a telescope. Gonna be in space.",
    "Come here, space. I have a secret for you. No, come closer.",
    "Dad! I'm in space! [low-pitched 'space' voice] I'm proud of you, son. [normal voice] Dad, are you space? [low-pitched 'space' voice] Yes. Now we are a family again.",
    "Don't like space. Don't like space.",
    "Earth earth earth.",
    "Earth.",
    "Getting bored of space.",
    "Going to space going there can't wait gotta go. Space. Going.",
    "Going to space.",
    "Gonna be in space.",
    "Gotta go to space. Lady. Lady.",
    "Gotta go to space. Yeah. Gotta go to space.",
    "Gotta go to space.",
    "Help me, space cops. Space cops, help.",
    "Hey hey hey hey hey!",
    "Hey lady. Lady. I'm the best. I'm the best at space.",
    "Hey lady. Lady.",
    "Hey lady.",
    "Hey.",
    "Hmmm. Hmmmmmm. Hmm. Hmmmmm. Space!",
    "I love space. Love space.",
    "I love space.",
    "I'm going to space.",
    "I'm in space.",
    "It's too big. Too big. Wanna go home. Wanna go to earth.",
    "Lady. I love space. I know! Spell it! S P... AACE. Space. Space.",
    "Lady.",
    "Let's go - let's go to space. Let's go to space.",
    "Look, an eclipse! No. Don't look.",
    "Love space. Need to go to space.",
    "Oh boy.",
    "Oh I know! I know I know I know I know I know - let's go to space!",
    "Oh oh oh oh. Wait wait. Wait I know. I know. I know wait. Space.",
    "Oh oh oh ohohohoh oh. Gotta go to space.",
    "Oh oh oh. This is space! I'm in space!",
    "Oh. Play cool. Play cool. Here come the space cops. Here come the space cops.",
    "Ohhh, the Sun. I'm gonna meet the Sun. Oh no! What'll I say? 'Hi! Hi, Sun!' Oh, boy!",
    "Ohhhh, space.",
    "Ohmygodohmygodohmygod! I'm in space!",
    "Oo. Oo. Oo. Lady. Oo. Lady. Oo. Let's go to space.",
    "Oooh! Ooh! Hi hi hi hi hi. Where we going? Where we going? Hey. Lady. Where we going? Where we going? Let's go to space!",
    "Orbit. Space orbit. In my spacesuit.",
    "Please go to space.",
    "So much space. Need to see it all.",
    "SPAAACCCCCE!",
    "SPAAACE!",
    "Space Court. For people in space. Judge space sun presiding. Bam. Guilty. Of being in space. I'm in space.",
    "Space going to space can't wait.",
    "Space space going to space oh boy",
    "Space space space. Going. Going there. Okay. I love you, space.",
    "Space space wanna go to space yes please space. Space space. Go to space.",
    "Space space wanna go to space",
    "Space wanna go wanna go to space wanna go to space",
    "Space!",
    "Space? SPACE!",
    "Space. Space. Go to space.",
    "Space. Space. Gonna go to space.",
    "Space. Space. Space. Space. Comets. Stars. Galaxies. Orion.",
    "Space. Space.",
    "Space. Trial. Puttin' the system on trial. In space. Space system. On trial. Guilty. Of being in space! Going to space jail!",
    "Space...",
    "Space.",
    "There's a star. There's another one. Star. Star star star. Star.",
    "Wait wait wait wait. I know I know I know. Lady wait. Wait. I know. Wait. Space.",
    "Wanna go home wanna go home wanna go home wanna go home.",
    "Wanna go home.",
    "Wanna go to -- wanna go to space",
    "Wanna go to earth wanna go to earth wanna go to earth wanna go to earth. Wanna go to earth.",
    "Wanna go to earth.",
    "Wanna go to space. Space.",
    "Wanna go to space.",
    "We are?",
    "We made it we made it we made it. Space!",
    "What's your favorite thing about space? Mine is space.",
    "Where am I? Guess. Guess guess guess. I'm in space.",
    "Yeah yeah yeah okay okay.",
    "Yeah, yeah, yeah, I'm going. Going to space.",
    "YEEEHAAAAAW!",
    "Yes. Please. Space.",
    "You are the farthest ever in space. Why me, space? Because you are the best. I'm the best at space? Yes.",
]
module.exports = spaceList