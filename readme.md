# Discord bot
For #Warframe server

## Baro
* Use `!baro` to trigger. Doesn't currently support any arguments

## Roll
Use `!roll die [, die...]` to trigger. 
* e.g. `!roll d20 d6`
* Displays the rolls and the total

## Sortie
* Grabs all the current sortie stages and displays mission type, condition, and node, along with time left

## To DO
* add embed features
* add arbitration 
* add easter egg arby